import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { MusicalGroupCardListModule } from './components/musical-group-card-list/musical-group-card-list.module';
import { MusicalGroupService } from './services/musical-group.service';
import { UploadXlsxModule } from './components/upload-xlsx/upload-xlsx.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MusicalGroupCardListModule,
    ReactiveFormsModule,
    UploadXlsxModule,
  ],
  providers: [MusicalGroupService],
  bootstrap: [AppComponent],
})
export class AppModule {}
