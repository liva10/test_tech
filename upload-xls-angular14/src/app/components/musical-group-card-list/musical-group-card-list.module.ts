import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MusicalGroupCardListComponent } from './musical-group-card-list.component';
import { MusicalGroupCardComponent } from './musical-group-card/musical-group-card.component';

@NgModule({
  declarations: [MusicalGroupCardListComponent, MusicalGroupCardComponent],
  imports: [CommonModule, MatCardModule],
  exports: [MusicalGroupCardListComponent],
})
export class MusicalGroupCardListModule {}
