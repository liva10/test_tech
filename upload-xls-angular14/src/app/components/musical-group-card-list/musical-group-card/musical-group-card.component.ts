import { Component, Input, OnInit } from '@angular/core';
import { IMusicalGroup } from 'src/app/interfaces/musical-group';

@Component({
  selector: 'app-musical-group-card',
  templateUrl: './musical-group-card.component.html',
  styleUrls: ['./musical-group-card.component.scss'],
})
export class MusicalGroupCardComponent {
  @Input() data!: IMusicalGroup;
}
