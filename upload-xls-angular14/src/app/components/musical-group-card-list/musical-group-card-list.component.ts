import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IMusicalGroup } from 'src/app/interfaces/musical-group';

@Component({
  selector: 'app-musical-group-card-list',
  templateUrl: './musical-group-card-list.component.html',
  styleUrls: ['./musical-group-card-list.component.scss'],
})
export class MusicalGroupCardListComponent implements OnChanges {
  @Input() data!: IMusicalGroup[];

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }
}
