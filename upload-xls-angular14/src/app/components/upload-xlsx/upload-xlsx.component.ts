import {
  Component,
  ElementRef,
  EventEmitter,
  Output,
  ViewChild,
} from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MusicalGroupService } from 'src/app/services/musical-group.service';

@Component({
  selector: 'app-upload-xlsx',
  templateUrl: './upload-xlsx.component.html',
  styleUrls: ['./upload-xlsx.component.scss'],
})
export class UploadXlsxComponent {
  file: any;
  @ViewChild('fileDropRef') fileDropRef!: ElementRef;

  @Output() refreshData = new EventEmitter();

  constructor(
    private musicalGroupService: MusicalGroupService,
    private _snackBar: MatSnackBar
  ) {}

  onFileDropped($event: any) {
    this.file = $event.target.files[0];
  }

  fileBrowseHandler($event: any) {
    this.file = $event.target.files[0];
  }

  deleteFile() {
    this.file = null;
    this.fileDropRef.nativeElement.value = null;
  }

  submitFile() {
    this.musicalGroupService.uploadXlsxMusicalGroup(this.file).subscribe({
      next: (data: any) => {
        this.openSnackBar(data.message);
        this.deleteFile();
        this.refreshData.emit();
      },
      error: (data) => this.openSnackBar(data.error.message.detail),
    });
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'Ok', {
      horizontalPosition: 'right',
      verticalPosition: 'top',
      duration: 2500,
    });
  }
}
