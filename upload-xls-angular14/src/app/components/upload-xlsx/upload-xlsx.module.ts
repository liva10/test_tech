import { NgModule } from '@angular/core';
import { UploadXlsxComponent } from './upload-xlsx.component';
import { MusicalGroupService } from 'src/app/services/musical-group.service';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [UploadXlsxComponent],
  imports: [CommonModule, MatButtonModule, MatSnackBarModule],
  providers: [MusicalGroupService],
  exports: [UploadXlsxComponent],
})
export class UploadXlsxModule {}
