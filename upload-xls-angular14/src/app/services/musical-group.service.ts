import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IMusicalGroup } from '../interfaces/musical-group';
import { environment } from 'src/environments/environment';

@Injectable()
export class MusicalGroupService {
  constructor(private http: HttpClient) {}

  getAllMusicalGroup(): Observable<IMusicalGroup[]> {
    return this.http.get<IMusicalGroup[]>(`${environment.apiKey}musical/group`);
  }

  uploadXlsxMusicalGroup(file: File) {
    const formData: any = new FormData();
    formData.append('file', file);

    return this.http.post(`${environment.apiKey}upload`, formData);
  }
}
