export interface IMusicalGroup {
  id: number;
  name: string;
  description: string;
  creator: string | null;
  membersCount: number | null;
  separationYear: number | null;
  startYear: number;
  country: {
    id: number;
    name: string;
  };
  city: {
    id: number;
    name: string;
  };
  musicGenre: {
    id: number;
    name: string;
  };
}
