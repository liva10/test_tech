import { Component, Input, OnInit } from '@angular/core';
import { MusicalGroupService } from './services/musical-group.service';
import { IMusicalGroup } from './interfaces/musical-group';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnInit {
  @Input() data!: IMusicalGroup[];

  constructor(private musicalGroupService: MusicalGroupService) {}

  ngOnInit(): void {
    this.fetchData();
  }

  fetchData(): void {
    this.musicalGroupService.getAllMusicalGroup().subscribe((data) => {
      this.data = data;
      console.log(this.data);
    });
  }
}
