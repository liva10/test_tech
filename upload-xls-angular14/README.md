# test_tech_Liva

**Development time:** `2 hours`

## Tech Stack

**Framework:** Angular 14.2.12

**Librairies:** Material 13

Firstly, you must installed an @Angular/cli@14, run

```bash
  npm install -g @Angular/cli@14
```

Install dependencies

```bash
  yarn install
```

Set the Backend URL in environments/environment file.

Run the app

```bash
  ng serve`
```
