# test_tech_Liva

**Development time:** `7 hours`

## Tech Stack

**Framework:** Symfony 5.4

**Librairies:** Doctrine 2, PhpspreadSheet 1.29

**Database:** Mysql 8.0.33

Firstly, you need to check your environment if it's ready to run symfony project

### symfony check:requirements

If there is an error, go to the symfony website https://symfony.com/doc/current/setup.html to prepare correctly your environment

To install the project, run first

```bash
composer install
```

Create a .env file based on the .env.example to set up your config

To create the database, run

```bash
php bin/console doctrine:database:create
```

To migrate the database, run

```bash
 php bin/console doctrine:migrations:migrate
```

Now you can start the server by running :

```bash
 symfony serve:start
```

Enjoy ;D !!!
