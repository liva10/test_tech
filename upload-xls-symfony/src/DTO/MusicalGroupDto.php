<?php

namespace App\DTO;

class MusicalGroupDto
{
  public int $id;
  public string $name;
  public string $description;
  public int $starYear;
  public ?string $creator;
  public ?int $membersCount;
  public ?int $separationYear;
  public CountryDto $country;
  public CityDto $city;
  public ?MusicGenreDto $musicGenre;
}
