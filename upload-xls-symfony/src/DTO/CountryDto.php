<?php 
namespace App\DTO;

class CountryDto 
{
  public int $id;
  public string $name;
}