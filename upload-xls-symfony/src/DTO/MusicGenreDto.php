<?php 
namespace App\DTO;

class MusicGenreDto 
{
  public int $id;
  public string $name;
}