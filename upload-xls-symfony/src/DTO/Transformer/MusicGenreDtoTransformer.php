<?php

namespace App\DTO\Transformer;

use App\DTO\MusicGenreDto;

class MusicGenreDtoTransformer extends AbstractResponseDtoTransformer
{

  public function transformFromObject($musicGenre)
  {

    if (null === $musicGenre) return null;

    $dto = new MusicGenreDto();
    $dto->id = $musicGenre->getId();
    $dto->name = $musicGenre->getName();

    return $dto;
  }
}
