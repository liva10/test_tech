<?php

namespace App\DTO\Transformer;

use App\DTO\MusicalGroupDto;

class MusicalGroupDtoTransformer extends AbstractResponseDtoTransformer
{

  public function __construct(
    private CountryDtoTransformer $countryDtoTransformer,
    private CityDtoTransformer $cityDtoTransformer,
    private MusicGenreDtoTransformer $musicGenreDtoTransformer,
  ) {
  }

  public function transformFromObject($musicalGroup)
  {

    $dto = new MusicalGroupDto();
    $dto->id = $musicalGroup->getId();
    $dto->name = $musicalGroup->getName();
    $dto->description = $musicalGroup->getDescription();
    $dto->startYear = $musicalGroup->getStartYear();
    $dto->creator = $musicalGroup->getCreator();
    $dto->membersCount = $musicalGroup->getMembersCount();
    $dto->separationYear = $musicalGroup->getSeparationYear();
    $dto->country = $this->countryDtoTransformer->transformFromObject($musicalGroup->getCountry());
    $dto->city = $this->cityDtoTransformer->transformFromObject($musicalGroup->getCity());
    $dto->musicGenre = $this->musicGenreDtoTransformer->transformFromObject($musicalGroup->getMusicGenre());

    return $dto;
  }
}
