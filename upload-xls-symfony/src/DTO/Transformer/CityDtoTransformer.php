<?php
namespace App\DTO\Transformer;

use App\DTO\CityDto;

class CityDtoTransformer extends AbstractResponseDtoTransformer
{
  public function transformFromObject($city) 
  {

    $dto = new CityDto();
    $dto->id = $city->getId();
    $dto->name = $city->getName();
    
    return $dto;
  }
}