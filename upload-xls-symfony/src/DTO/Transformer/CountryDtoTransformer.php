<?php
namespace App\DTO\Transformer;

use App\DTO\CountryDto;

class CountryDtoTransformer extends AbstractResponseDtoTransformer
{
  public function transformFromObject($country) 
  {

    $dto = new CountryDto();
    $dto->id = $country->getId();
    $dto->name = $country->getName();
    
    return $dto;
  }
}