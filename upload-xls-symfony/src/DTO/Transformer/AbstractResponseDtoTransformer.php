<?php
namespace App\DTO\Transformer;

use App\DTO\Transformer\ResponseDtoTransformerInterface;

abstract class AbstractResponseDtoTransformer implements ResponseDtoTransformerInterface
 {
  public function transformFromObjects(iterable $objects): iterable
  {
    $dto = [];
    foreach($objects as $object) {
      
      $dto[] = $this->transformFromObject($object);
    }
    
    return $dto;
  }
 }