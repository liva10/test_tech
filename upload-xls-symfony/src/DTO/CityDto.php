<?php 
namespace App\DTO;

class CityDto 
{
  public int $id;
  public string $name;
}