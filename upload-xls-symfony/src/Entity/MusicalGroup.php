<?php

namespace App\Entity;

use App\Repository\MusicalGroupRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: MusicalGroupRepository::class)]
class MusicalGroup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;



    #[ORM\ManyToOne(inversedBy: 'musicalGroups')]
    private ?MusicGenre $musicGenre = null;

    #[ORM\ManyToOne(inversedBy: 'musicalGroups')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotNull]
    private ?Country $country = null;

    #[ORM\ManyToOne(inversedBy: 'musicalGroups')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotNull]
    private ?City $city = null;

    public function __construct(
        #[ORM\Column(length: 255)]
        #[Assert\NotBlank]
        private string $name,

        #[ORM\Column]
        #[Assert\Range(
            min: 1800,
            max: 2050
        )]
        #[Assert\NotBlank]
        private int $startYear,

        #[ORM\Column(type: 'text')]
        #[Assert\NotBlank]
        private string $description,

        #[ORM\Column(length: 255, nullable: true)]
        private ?string $creator = null,

        #[ORM\Column(nullable: true)]
        #[Assert\Positive]
        private ?int $membersCount = null,

        #[ORM\Column(nullable: true)]
        #[Assert\Range(
            min: 1800,
            max: 2050
        )]
        private ?int $separationYear = null,


    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getStartYear(): int
    {
        return $this->startYear;
    }

    public function setStartYear(int $startYear): static
    {
        $this->startYear = $startYear;

        return $this;
    }

    public function getCreator(): ?string
    {
        return $this->creator;
    }

    public function setCreator(?string $creator): static
    {
        $this->creator = $creator;

        return $this;
    }

    public function getMembersCount(): ?int
    {
        return $this->membersCount;
    }

    public function setMembersCount(?int $membersCount): static
    {
        $this->membersCount = $membersCount;

        return $this;
    }

    public function getSeparationYear(): ?int
    {
        return $this->separationYear;
    }

    public function setSeparationYear(?int $separationYear): static
    {
        $this->separationYear = $separationYear;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getMusicGenre(): ?MusicGenre
    {
        return $this->musicGenre;
    }

    public function setMusicGenre(?MusicGenre $musicGenre): static
    {
        $this->musicGenre = $musicGenre;

        return $this;
    }

    public function getCountry(): Country
    {
        return $this->country;
    }

    public function setCountry(Country $country): static
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function setCity(City $city): static
    {
        $this->city = $city;

        return $this;
    }
}
