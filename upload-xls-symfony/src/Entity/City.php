<?php

namespace App\Entity;

use App\Repository\CityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CityRepository::class)]
class City
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'city', targetEntity: MusicalGroup::class)]
    private Collection $musicalGroups;

    public function __construct()
    {
        $this->musicalGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, MusicalGroup>
     */
    public function getMusicalGroups(): Collection
    {
        return $this->musicalGroups;
    }

    public function addMusicalGroup(MusicalGroup $musicalGroup): static
    {
        if (!$this->musicalGroups->contains($musicalGroup)) {
            $this->musicalGroups->add($musicalGroup);
            $musicalGroup->setCity($this);
        }

        return $this;
    }

    public function removeMusicalGroup(MusicalGroup $musicalGroup): static
    {
        if ($this->musicalGroups->removeElement($musicalGroup)) {
            // set the owning side to null (unless already changed)
            if ($musicalGroup->getCity() === $this) {
                $musicalGroup->setCity(null);
            }
        }

        return $this;
    }
}
