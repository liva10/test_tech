<?php

namespace App\Controller;

use App\Repository\MusicalGroupRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\DTO\Transformer\MusicalGroupDtoTransformer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MusicalGroupController extends AbstractController
{

    public function __construct(
        private MusicalGroupRepository $musicalGroupRepository,
        private MusicalGroupDtoTransformer $musicalGroupDtoTransformer,
        private SerializerInterface $serializer,
    ) {}

    #[Route('/musical/group', name: 'app_musical_group')]
    public function index(): JsonResponse
    {
        $data = $this->musicalGroupRepository->findAll();
        $jsonData = $this->musicalGroupDtoTransformer->transformFromObjects($data);

        return new JsonResponse($jsonData);
    }
}
