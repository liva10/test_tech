<?php

namespace App\Controller;

use App\Manager\ParserManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UploadController extends AbstractController
{
    public function __construct(private ParserManager $parserManager)
    {
    }

    #[Route('/upload', name: 'app_upload', methods: ['POST'])]
    public function index(Request $request): Response
    {
        $file = $request->files->get('file');

        $data = $this->parserManager->parseXlsxToMusicalGroup($file);

        return $this->json([
            'message' => $data["message"],
            'success' => $data["success"],
        ], $data['code']);
    }
}
