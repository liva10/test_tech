<?php

namespace App\Manager;


use App\Utils\XlsxHeader;
use App\Entity\MusicalGroup;
use App\Services\CityService;
use App\Services\CountryService;
use App\Services\MusicGenreService;
use App\Utils\XlsxData;
use App\Utils\XlsxDataColumn;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ParserManager
{
    final public const HEADER_ROW_NUMBER = 1;

    public function __construct(
        private CountryService $countryService,
        private CityService $cityService,
        private MusicGenreService $musicGenreService,
        private EntityManagerInterface $entityManager,
        private XlsxHeader $xlsxHeader,
        private XlsxData $xlsxData,
        private XlsxDataColumn $xlsxDataColumn,
        private ValidatorInterface $validator,
    ) {
    }

    public function parseXlsxToMusicalGroup(
        UploadedFile $file
    ): array {
        $reader = new Xlsx();
        $reader = new Xlsx();
        $indexHeaders = [];

        try {
            $spreadsheet = $reader->load($file->getRealPath());
            $worksheet = $spreadsheet->getActiveSheet();
            $indexHeaders = $this->xlsxHeader->getHeaderIndex($worksheet);

            $rowIndex = self::HEADER_ROW_NUMBER + 1;
            $rowsCount = $worksheet->getHighestRow();

            $countriesName = $this->xlsxDataColumn->getUniqueValues(
                worksheet: $worksheet,
                col: $indexHeaders[XlsxHeader::COL_COUNTRY],
                rowIndex: $rowIndex,
            );
            $countries = $this->countryService->getAllEntities($countriesName);
            $citiesName = $this->xlsxDataColumn->getUniqueValues(
                worksheet: $worksheet,
                col: $indexHeaders[XlsxHeader::COL_CITY],
                rowIndex: $rowIndex,
            );
            $cities = $this->cityService->getAllEntities($citiesName);

            $musicGenresName = $this->xlsxDataColumn->getUniqueValues(
                worksheet: $worksheet,
                col: $indexHeaders[XlsxHeader::COL_MUSIC_GENRE],
                rowIndex: $rowIndex,
            );
            $musicGenres = $this->musicGenreService->getAllEntities($musicGenresName);

            for ($i = $rowIndex; $i <= $rowsCount; $i++) {
                $musicalGroup = $this->parseMusicalGroup($worksheet, $indexHeaders, $countries, $cities, $musicGenres, $i);
                $countries = $this->addToArray($countries, $musicalGroup->getCountry());
                $cities = $this->addToArray($cities, $musicalGroup->getCity());
                $musicGenres = $this->addToArray($musicGenres, $musicalGroup->getMusicGenre());

                $errors = $this->validator->validate($musicalGroup);

                if (0 < \count($errors)) {
                    return [
                        'message' => $errors,
                        'success' => false,
                        'code' => JsonResponse::HTTP_BAD_REQUEST,
                    ];
                }

                $this->entityManager->persist($musicalGroup);
            }

            if (1 < $rowsCount) {
                $this->entityManager->flush();

                return [
                    'message' => sprintf("Succès : %d lignes sauvegardées en base de données", $rowsCount - 1),
                    'success' => true,
                    'code' => JsonResponse::HTTP_ACCEPTED,
                ];
            }
        } catch (\Exception $e) {
            return [
                'message' => $e->getMessage(),
                'success' => false,
                'code' => JsonResponse::HTTP_NOT_ACCEPTABLE,
            ];
        }
    }

    private function parseMusicalGroup(
        Worksheet $worksheet,
        array $indexHeaders,
        array $countries,
        array $cities,
        array $musicGenres,
        int $row
    ): MusicalGroup {
        $musicalGroup = new MusicalGroup(
            name: $this->xlsxData->getCellValue($worksheet, $indexHeaders[XlsxHeader::COL_NAME], $row),
            startYear: $this->xlsxData->getCellIntValue($worksheet, $indexHeaders[XlsxHeader::COL_START_YEAR], $row),
            description: $this->xlsxData->getCellValue($worksheet, $indexHeaders[XlsxHeader::COL_DESCRIPTION], $row),
            creator: $this->xlsxData->getCellValue($worksheet, $indexHeaders[XlsxHeader::COL_CREATOR], $row),
            membersCount: $this->xlsxData->getCellIntValue($worksheet, $indexHeaders[XlsxHeader::COL_MEMBER_COUNT], $row),
            separationYear: $this->xlsxData->getCellIntValue($worksheet, $indexHeaders[XlsxHeader::COL_SEPARATION_YEAR], $row),
        );

        $colCountryValue = $this->xlsxData->getCellValue($worksheet, $indexHeaders[XlsxHeader::COL_COUNTRY], $row);
        $country = $this->countryService->getCountryByName($countries, $colCountryValue)
            ?? $this->countryService->addNewCountry($colCountryValue);
        $musicalGroup->setCountry($country);

        $colCityValue = $this->xlsxData->getCellValue($worksheet, $indexHeaders[XlsxHeader::COL_CITY], $row);
        $city = $this->cityService->getCityByName($cities, $colCityValue)
            ?? $this->cityService->addNewCity($colCityValue);
        $musicalGroup->setCity($city);

        $colMusicGenreValue = $this->xlsxData->getCellValue($worksheet, $indexHeaders[XlsxHeader::COL_MUSIC_GENRE], $row);
        $musicGenre = $this->musicGenreService->getMusicGenreByName($musicGenres, $colMusicGenreValue)
            ?? $this->musicGenreService->addNewMusicGenre($colMusicGenreValue);
        $musicalGroup->setMusicGenre($musicGenre);

        return $musicalGroup;
    }

    private function addToArray(array $entities, ?Object $entity): array
    {
        if (null === $entity || in_array($entity, $entities)) {
            return $entities;
        }

        $entities[] = $entity;

        return $entities;
    }
}
