<?php

namespace App\Services;

use App\Entity\Country;
use App\Repository\CountryRepository;
use Doctrine\ORM\EntityManagerInterface;

class CountryService implements DataRetrievalServiceInterface
{
  public function __construct(
    private EntityManagerInterface $entityManager,
    private CountryRepository $countryRepository,
  ) {
  }

  public function getAllEntities(array $citiesName): array
  {
    return $this->countryRepository->findBy(['name' => $citiesName]);
  }

  public function getCountryByName(array $countries, string $countryName): ?Country
  {
    foreach ($countries as $country) {
      if ($country->getName() === $countryName) {
        return $country;
      }
    }

    return null;
  }

  public function addNewCountry(string $countryValue)
  {

    $country = new Country();
    $country->setName($countryValue);
    $this->entityManager->persist($country);
    $this->entityManager->flush();

    return $country;
  }
}
