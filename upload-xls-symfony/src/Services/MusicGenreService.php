<?php

namespace App\Services;

use App\Entity\MusicGenre;
use App\Repository\MusicGenreRepository;
use Doctrine\ORM\EntityManagerInterface;

class MusicGenreService implements DataRetrievalServiceInterface
{
  public function __construct(
    private EntityManagerInterface $entityManager,
    private MusicGenreRepository $musicGenreRepository,
  ) {
  }

  public function getAllEntities(array $musicGenresName): array
  {

    return $this->musicGenreRepository->findBy(['name' => $musicGenresName]);
  }

  public function getMusicGenreByName(array $musicGenres, string $musicGenreName): ?MusicGenre
  {

    foreach ($musicGenres as $musicGenre) {
      if ($musicGenre->getName() === $musicGenreName) {
        return $musicGenre;
      }
    }


    return null;
  }

  public function addNewMusicGenre(string $musicGenreName)
  {
    if ("" === $musicGenreName) {
      return null;
    }

    $musicGenre = new MusicGenre();
    $musicGenre->setName($musicGenreName);
    $this->entityManager->persist($musicGenre);
    $this->entityManager->flush();

    return $musicGenre;
  }
}
