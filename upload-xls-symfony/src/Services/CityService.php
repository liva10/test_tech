<?php

namespace App\Services;

use App\Entity\City;
use App\Repository\CityRepository;
use Doctrine\ORM\EntityManagerInterface;

class CityService implements DataRetrievalServiceInterface
{
  public function __construct(
    private EntityManagerInterface $entityManager,
    private CityRepository $cityRepository,
  ) {
  }

  public function getAllEntities(array $citiesName): array
  {
    return $this->cityRepository->findBy(['name' => $citiesName]);
  }

  public function getCityByName(array $cities, string $cityName): ?City
  {
    foreach ($cities as $city) {
      if ($city->getName() === $cityName) {
        return $city;
      }
    }

    return null;
  }


  public function addNewCity(string $cityName)
  {
    $city = new City();
    $city->setName($cityName);
    $this->entityManager->persist($city);
    $this->entityManager->flush();

    return $city;
  }
}
