<?php

namespace App\Services;

interface DataRetrievalServiceInterface
{
  public function getAllEntities(array $name): array;
}
