<?php

namespace App\Utils;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class XlsxHeader
{
  final public const COL_NAME = "Nom du groupe";
  final public const COL_START_YEAR = "Année début";
  final public const COL_CREATOR = "Fondateurs";
  final public const COL_MEMBER_COUNT = "Membres";
  final public const COL_SEPARATION_YEAR = "Année séparation";
  final public const COL_DESCRIPTION = "Présentation";
  final public const COL_MUSIC_GENRE = "Courant musical";
  final public const COL_COUNTRY = "Origine";
  final public const COL_CITY = "Ville";

  final public const HEADER_ROW_NUMBER = 1;

  public function getHeaderIndex(Worksheet $worksheet): array
  {
    $cellIterator = $worksheet->getRowIterator(self::HEADER_ROW_NUMBER)->current()->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(false);
    $columnIndex = 1;
    $headerColumns = [
      "COL_NAME" => self::COL_NAME,
      "COL_START_YEAR" => self::COL_START_YEAR,
      "COL_CREATOR" => self::COL_CREATOR,
      "COL_MEMBER_COUNT" => self::COL_MEMBER_COUNT,
      "COL_SEPARATION_YEAR" => self::COL_SEPARATION_YEAR,
      "COL_DESCRIPTION" => self::COL_DESCRIPTION,
      "COL_MUSIC_GENRE" => self::COL_MUSIC_GENRE,
      "COL_COUNTRY" => self::COL_COUNTRY,
      "COL_CITY" => self::COL_CITY,
    ];

    foreach ($cellIterator as $cell) {
      $cellValue = $cell->getValue();

      if (in_array($cellValue, $headerColumns)) {
        $indexHeaders[$cellValue] = $columnIndex;
      }
      $columnIndex++;
    }

    return $indexHeaders;
  }
}
