<?php

namespace App\Utils;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class XlsxData
{
  public function getCellValue(Worksheet $worksheet, int $col, int $row): string
  {

    return trim($worksheet->getCellByColumnAndRow($col, $row)->getValue());
  }

  public function getCellIntValue(Worksheet $worksheet, int $col, int $row): ?int
  {
    $colValue = self::getCellValue($worksheet, $col, $row);

    if ("" === $colValue) return null;

    return intval(self::getCellValue($worksheet, $col, $row));
  }
}
