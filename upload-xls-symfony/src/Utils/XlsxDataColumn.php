<?php

namespace App\Utils;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class XlsxDataColumn
{
  public function __construct(
    private XlsxData $xlsdata,
  ) {
  }

  public function getUniqueValues(Worksheet $worksheet, int $col, int $rowIndex): array
  {
    $names = [];
    $rowsCount = $worksheet->getHighestRow();

    for ($i = $rowIndex; $i < $rowsCount; $i++) {
      $colValue = $this->xlsdata->getCellValue($worksheet, $col, $i);

      if ("" !== $colValue && !in_array($colValue, $names)) {
        $names[] = $colValue;
      }
    }

    return $names;
  }
}
