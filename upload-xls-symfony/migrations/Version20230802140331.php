<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230802140331 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'ALL TABLES FOR ARTISTE GROUP UPLOAD';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE music_genre (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE musical_group (id INT AUTO_INCREMENT NOT NULL, music_genre_id INT DEFAULT NULL, country_id INT NOT NULL, city_id INT NOT NULL, name VARCHAR(255) NOT NULL, start_year INT NOT NULL, description LONGTEXT NOT NULL, creator VARCHAR(255) DEFAULT NULL, members_count INT DEFAULT NULL, separation_year INT DEFAULT NULL, INDEX IDX_D3F3232185858F39 (music_genre_id), INDEX IDX_D3F32321F92F3E70 (country_id), INDEX IDX_D3F323218BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE musical_group ADD CONSTRAINT FK_D3F3232185858F39 FOREIGN KEY (music_genre_id) REFERENCES music_genre (id)');
        $this->addSql('ALTER TABLE musical_group ADD CONSTRAINT FK_D3F32321F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE musical_group ADD CONSTRAINT FK_D3F323218BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE musical_group DROP FOREIGN KEY FK_D3F3232185858F39');
        $this->addSql('ALTER TABLE musical_group DROP FOREIGN KEY FK_D3F32321F92F3E70');
        $this->addSql('ALTER TABLE musical_group DROP FOREIGN KEY FK_D3F323218BAC62AF');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE music_genre');
        $this->addSql('DROP TABLE musical_group');
    }
}
